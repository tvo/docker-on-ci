ARG BASE_ARCH=mcr.microsoft.com/dotnet/core/sdk:3.1
FROM ${BASE_ARCH}
RUN dotnet --version
ENTRYPOINT ["dotnet", "--version"]
